﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {
	[SerializeField]
	private float speed = 4f;

	[SerializeField]
	private Character target;

	private void Awake() {
		if (!target)
			target = FindObjectOfType<Character> ();
	}

	private void Start() {
		Vector3 position = target.transform.position;
		position.y += 3f;
		position.z = -10;

		transform.position = position;
	}

	void Update () {
		Vector3 position = transform.position;
		position.x = target.transform.position.x;
		if (target.isGrounded)
			position.y = target.transform.position.y + 3f;
		transform.position = Vector3.Lerp (transform.position, position, speed * Time.deltaTime);
	}
}
