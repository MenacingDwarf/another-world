﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootableMonster : Monster {
	[SerializeField]
	private float rate = 1f;

	private Bullet bullet;

	private bool enableShoot = true;

	private SpriteRenderer sprite;

	protected override void Awake() {
		bullet = Resources.Load<Bullet> ("Bullet");
		sprite = GetComponentInChildren<SpriteRenderer> ();
	}
	protected override void Update ()
	{
		if (enableShoot)
			StartCoroutine (Shoot ());
	}

	private IEnumerator Shoot() {
		enableShoot = false;
		Vector3 position = transform.position + transform.up * 0.5f;
		Bullet newBullet = Instantiate (bullet, position, bullet.transform.rotation) as Bullet;

		newBullet.ParentTag = gameObject.tag;
		newBullet.Direction = newBullet.transform.right * (sprite.flipX ? 1.0f : -1.0f);
		newBullet.color = Color.black;

		yield return new WaitForSeconds (rate);
		enableShoot = true;
	}

}
