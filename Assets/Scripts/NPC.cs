﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Unit {

	public Dilogue dilogue;

	public SpriteRenderer sprite;

	private Character player;

	private void Awake() {
		player = FindObjectOfType<Character> ();
		sprite = GetComponentInChildren<SpriteRenderer> ();
	}

	private void Update() {
		DilogManager dilogManager = FindObjectOfType<DilogManager> ();
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position, 1f);
		for (int i = 0; i < colliders.Length; i++)
			if (colliders [i].tag == "Player") {
				flip (colliders [i].transform.position);
				if (!dilogManager.isStarted && Input.GetButtonDown("Fire2")) {
					dilogManager.StartDilogue (dilogue);
					dilogue.sentences = new string[1];
					dilogue.sentences [0] = "Go Away! Faster!";
				}
			}
	}

	private void flip(Vector3 position) {
		sprite.flipX = transform.position.x > position.x;
	}

	/*private void OnTriggerEnter2D (Collider2D other) {
		Character character = other.GetComponent<Character> ();
		Debug.Log ("dilogue");
		if (character && Input.GetKey(KeyCode.F)) {
			Debug.Log ("dilogue");
			FindObjectOfType<DilogManager>().StartDilogue(dilogue);
		}
	}*/
}
