﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossEntry : MonoBehaviour {
	[SerializeField]
	private int LevelNumber;
	[SerializeField]
	private string LevelName;
	[SerializeField]
	private int NeededKeys = 3;

	[SerializeField]
	private Text message;

	private IEnumerator ShowMessage(string text) {
		message.text = text;
		message.GetComponent<Animator> ().SetBool ("Visible", true);

		yield return new WaitForSeconds(1.5f);

		message.GetComponent<Animator> ().SetBool ("Visible", false);
	}


	void Update() {
		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position, 1f);

		for (int i = 0; i < colliders.Length; i++)
			if (colliders [i].tag == "Player" && Input.GetButtonDown ("Fire2")){
				if (PlayerPrefs.GetInt ("KeysNumber") < NeededKeys)
					StartCoroutine (ShowMessage ("You need more keys to go in"));
				else {
					PlayerPrefs.SetInt ("KeysNumber", 0);
					Application.LoadLevel (LevelNumber);
				}
			}
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player")
			StartCoroutine(ShowMessage(LevelName));
	}
}
