﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePlatform : MonoBehaviour {

	public float moveTime = 1f; 
	public float speed = 2f;

	private Vector3 direction;

	[SerializeField]
	private bool horizontal;

	// Use this for initialization
	private void Awake() {
		if (!horizontal)
			direction = transform.up;
		else
			direction = transform.right;
		StartCoroutine (changeDirection (moveTime));
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	void Move() {
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);

		Collider2D[] colliders = Physics2D.OverlapBoxAll (transform.position, new Vector2 (5.3f,0.4f), 0f);
		foreach (Collider2D collider in colliders)
			if (collider.gameObject != gameObject) collider.transform.position = Vector3.MoveTowards (collider.transform.position, collider.transform.position + direction, speed * Time.deltaTime);
	}

	private IEnumerator changeDirection(float time) {
		yield return new WaitForSeconds (time);
		direction *= -1f;
		StartCoroutine (changeDirection (moveTime * 2));
	}
}
