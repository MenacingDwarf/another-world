﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exit : MonoBehaviour {
	[SerializeField]
	private int needPoints = 0;
	[SerializeField]
	private string nextLevelName;

	private string[] WithoutKeyLevels = new string[]{"First Appearance", "Very bad boy"};

	[SerializeField]
	private Text message;

	private IEnumerator ShowMessage(string text) {
		message.text = text;
		message.GetComponent<Animator> ().SetBool ("Visible", true);

		yield return new WaitForSeconds(1.5f);

		message.GetComponent<Animator> ().SetBool ("Visible", false);
	}

	private void OnTriggerEnter2D (Collider2D other) {
		Character player = other.GetComponent<Character> ();

		if (player.tag == "Player") {
			if (!player.hasKey)
				StartCoroutine (ShowMessage ("You need a key!"));
			else if (player.Points < needPoints)
				StartCoroutine (ShowMessage ("You need to kill all monsters!"));
			
			else {
				if (!PlayerPrefs.HasKey("Level" + Application.loadedLevel.ToString())){
					PlayerPrefs.SetInt ("Level" + Application.loadedLevel.ToString(),0);
					bool giveKey = true;
					for (int i = 0; i < WithoutKeyLevels.Length; i++)
						if (Application.loadedLevelName == WithoutKeyLevels [i])
							giveKey = false;
					if (giveKey)
						PlayerPrefs.SetInt ("KeysNumber", PlayerPrefs.GetInt ("KeysNumber") + 1);
					PlayerPrefs.Save ();
				}
				if (nextLevelName == "Next")
					Application.LoadLevel (Application.loadedLevel + 1);
				else {
					if (nextLevelName == "Menu" && FindObjectOfType<MusicPlayer> ())
						Destroy(FindObjectOfType<MusicPlayer> ().gameObject);
					Application.LoadLevel (nextLevelName);
				}
			}

		}
	}
}
