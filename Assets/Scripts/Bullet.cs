﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private float speed = 15f;
	private Vector3 direction;

	private string parentTag;
	public string ParentTag { 
		get { return parentTag; }
		set { parentTag = value; } }

	private SpriteRenderer sprite;

	public Color color {
		set { sprite.color = value; }
	}


	public Vector3 Direction { 
		set { direction = value; }
		get { return direction; }
	}

	private void Awake() {
		sprite = GetComponentInChildren<SpriteRenderer> ();
	}

	private void Start() {
		Destroy (gameObject, 0.6f);
	}

	private void Update() {
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);
	}

	private void OnTriggerEnter2D (Collider2D other) {
		Unit unit = other.GetComponent<Unit> ();

		if (other.tag != ParentTag) {
			if (unit && unit is Character) unit.ReceiveDamage ();
			Destroy (gameObject);
		}
	}
}
