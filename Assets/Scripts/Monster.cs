﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Unit {

	protected virtual void Awake() {
	}
	protected virtual void Start() {
	}
	protected virtual void Update() {
	}

	protected virtual void OnTriggerEnter2D (Collider2D other) {
		Bullet bullet = other.GetComponent<Bullet> ();
		if (bullet && bullet.ParentTag != gameObject.tag) { 
			ReceiveDamage ();
		}

		Unit unit = other.GetComponent<Unit> ();

		if (unit && unit is Character) {
			unit.ReceiveDamage ();
		}
	}

	protected override void Die ()
	{
		FindObjectOfType<Character> ().Points++;
		base.Die ();
	}
}
