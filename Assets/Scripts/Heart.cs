﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : Item {

	protected override void OnTriggerEnter2D(Collider2D other) {
		Character player = other.GetComponent<Character> ();

		if (player && player.Lives < 5) {
			player.Lives++;
			player.GetComponent<AudioSource>().clip = destroySound;
			player.GetComponent<AudioSource>().Play ();
			Destroy (gameObject);
		}
	}

}
