﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MovableMonster : Monster {

	[SerializeField]
	private float speed = 2f;
	[SerializeField]
	private bool goLeft;

	private Vector3 direction;
	public Vector3 Direction {
		get { return direction; }
		set { direction = value; }
	}

	private Animator animator;
	private SpriteRenderer sprite;

	protected override void Start() {
		direction = transform.right * (goLeft ? -1f : 1f);
		animator = GetComponent<Animator> ();
		sprite = GetComponentInChildren<SpriteRenderer> ();
	}

	protected override void Update ()
	{
		Move ();
	}

	protected virtual void OnTriggerEnter2D (Collider2D other) {
	}

	private void Move() {
		animator.SetInteger ("State", 1);
		sprite.flipX = direction.x > 0f;

		Collider2D[] colliders = Physics2D.OverlapCircleAll (transform.position + transform.up * 0.5f + transform.right * direction.x * .5f, 0.1f);

		if (colliders.Length > 1 && colliders.All (x => x.tag!="Player" && x.tag!="Item" && x.tag != "Bullet")) {
			direction *= -1f;
		} 

		CheckColliders ();

		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);
	}
		

	void CheckColliders() {
		Collider2D[] colliders = Physics2D.OverlapBoxAll (transform.position + transform.up * 0.5f, new Vector2(0.85f,1f),0f);

		foreach (Collider2D collider in colliders) {
			if (collider.tag == "Player") {
				if (Mathf.Abs (transform.position.x - collider.GetComponent<Character> ().transform.position.x) < 0.85f && transform.position.y < collider.transform.position.y && !collider.GetComponent<Character>().IsDamaging) {
					ReceiveDamage ();
					collider.GetComponent<Character> ().Jump (collider.GetComponent<Character> ().jumpForse);
				}
				else
					collider.GetComponent<Character> ().ReceiveDamage ();
			}
		}
	}
}