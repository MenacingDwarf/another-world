﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : Unit {
	public enum CharState {
		Idle,
		Run,
		Jump
	}

	[SerializeField]
	protected float speed = 5f;
	[SerializeField]
	protected int lives = 5;
	private int points = 0;
	protected float shootRate = 0.5f;

	public int Points {
		get { return points; }
		set { points = value; }
	}

	public int Lives {
		get {return lives; }
		set {if (value <= 5) lives = value;  livesBar.Refresh (lives); }
	}
	[SerializeField]
	public float jumpForse = 10f;

	protected Rigidbody2D rb2D;
	protected Animator animator;
	protected SpriteRenderer sprite;
	[SerializeField]
	protected LivesBar livesBar;
	public Text message;
	protected AudioSource audio;

	[SerializeField]
	protected AudioClip shootSound;
	[SerializeField]
	protected AudioClip jumpSound;

	public Dilogue dilogue;
	protected DilogManager dilogueManager;
	protected SceneManager sceneManager;

	protected Bullet bullet;

	public CharState State {
		get { return (CharState)animator.GetInteger ("State"); }
		set { animator.SetInteger ("State", (int)value); }
	}

	[HideInInspector]
	public bool isGrounded = true;
	[SerializeField]
	protected bool hasStartedDilogue;
	protected bool isDamaging = false;
	public bool IsDamaging {
		get { return isDamaging; }
	}
	protected bool enableShoot = true;
	public bool hasKey = false;

	protected virtual void Awake() {
		dilogueManager = FindObjectOfType<DilogManager> ();
		sceneManager = FindObjectOfType<SceneManager> ();
		if (hasStartedDilogue)
			dilogueManager.StartDilogue (dilogue);

		rb2D = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
		animator.SetInteger ("State", 0);
		sprite = GetComponentInChildren<SpriteRenderer> ();
		audio = GetComponent<AudioSource> ();

		bullet = Resources.Load<Bullet> ("Bullet");
	}

	protected void ShowMessage(string text) {
		StartCoroutine(sceneManager.ShowMessage (text));
	}

	private IEnumerator Restart() {
		message.text = "GameOver";
		message.GetComponent<Animator> ().SetBool ("Visible", true);
		isDamaging = true;

		yield return new WaitForSeconds(2f);

		isDamaging = false;
		message.GetComponent<Animator> ().SetBool ("Visible", false);
		Application.LoadLevel (Application.loadedLevelName);
	}

	protected void FixedUpdate() {
		checkGround ();
	}

	protected virtual void Update() {
		if (!isDamaging && !dilogueManager.isStarted && Time.timeScale>0) {
			if (isGrounded) {
				State = CharState.Idle;
			}

			if (Input.GetButtonDown ("Fire1") && enableShoot)
				StartCoroutine (Shoot (Color.white));
			if (Input.GetAxis ("Horizontal") != 0)
				Run ();
			if (Input.GetButtonDown ("Jump") && isGrounded)
				Jump (jumpForse);
		}
		if (dilogueManager.isStarted) {
			State = CharState.Idle;
		}
	}

	protected virtual void Run() {
		if (isGrounded) State = CharState.Run;

		Vector3 direction = Input.GetAxis ("Horizontal") * transform.right;
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);

		sprite.flipX = direction.x < 0f;
	}

	public void Jump(float force) {
		audio.clip = jumpSound;
		if (PlayerPrefs.HasKey ("SoundVolume"))
			audio.volume = PlayerPrefs.GetInt ("SoundVolume") / 100f;
		else
			audio.volume = 1;
		audio.Play ();

		rb2D.velocity = Vector3.zero;
		rb2D.AddForce (transform.up * force, ForceMode2D.Impulse);
	}

	protected IEnumerator Shoot(Color color) {
		audio.clip = shootSound;
		audio.volume = PlayerPrefs.GetInt ("SoundVolume") / 100f;
		audio.Play ();

		enableShoot = false;
		Vector3 position = transform.position;
		position.y += 0.75f;
		Bullet newBullet = Instantiate (bullet, position, bullet.transform.rotation);

		newBullet.ParentTag = gameObject.tag;
		newBullet.Direction = newBullet.transform.right * (sprite.flipX ? -1.0f : 1.0f);
		newBullet.color = color;

		yield return new WaitForSeconds (shootRate);
		enableShoot = true;
	}

	protected void checkGround() {
		if (!isGrounded)
			animator.SetInteger ("State", 2);

		Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.3f);
		isGrounded = false;
		for (int i = 0; i < colliders.Length; i++)
			if (colliders [i].tag == "Ground")
				isGrounded = true;
	}

	public override void ReceiveDamage() {
		if (!isDamaging) {
			Lives--;
			if (lives <= 0) {
				Die ();
			} else {

				rb2D.velocity = Vector3.zero;

				Vector3 dir;
				if (sprite.flipX)
					dir = transform.right * 5f;
				else
					dir = -transform.right * 5f;
		
				rb2D.AddForce (transform.up * 10f + dir, ForceMode2D.Impulse);

				StartCoroutine (GetRed ());

			}
		}
	}

	protected override void Die ()
	{
		livesBar.gameObject.SetActive (false);
		StartCoroutine (Restart ());
	}

	protected IEnumerator GetRed () {
		isDamaging = true;
		sprite.color = Color.red;
		yield return new WaitForSeconds (0.5f);
		sprite.color = Color.white;
		isDamaging = false;
	}
}


