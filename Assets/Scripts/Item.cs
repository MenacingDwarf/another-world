﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
	[SerializeField]
	protected AudioClip destroySound;

	protected virtual void OnTriggerEnter2D(Collider2D other) {
		Character player = other.GetComponent<Character> ();
		if (player) {
			Effect (player);
			player.GetComponent<AudioSource>().clip = destroySound;
			player.GetComponent<AudioSource>().Play ();
			Destroy (gameObject);
		}
	}

	protected virtual void Effect(Character player) {
		
	}
}
