﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : Item {


	void Update () {
		transform.Rotate (new Vector3 (0, 2, 0));
	}

	protected override void Effect (Character player)
	{
		player.hasKey = true;
	}
}
