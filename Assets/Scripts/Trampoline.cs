﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour {

	[SerializeField]
	private float mult = 2f;

	private void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {
			if (other.GetComponent<Rigidbody2D>().velocity.y < 0 && other.GetComponent<Character> ().State == Character.CharState.Jump) other.GetComponent<Character> ().Jump (other.GetComponent<Character> ().jumpForse * mult);
		}
		if (other.gameObject.GetComponent<MovableMonster> ()) {
			other.gameObject.GetComponent<MovableMonster> ().Direction *= -1;
		}
	}
}
