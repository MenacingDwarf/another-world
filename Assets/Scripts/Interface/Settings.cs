﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;

public class Settings : MonoBehaviour {

	public Slider resolutionSlider;
	public Slider qualitySlider;
	public Slider musicSlider;
	public Slider soundSlider;
	public Toggle fullscreen;
	Resolution [] res;
	string[] qualityLevels = new string[]{"Very Low","Low","Medium","High","Very High", "Ultra"};

	void Start () {
		Resolution[] resolution = Screen.resolutions;
		res = resolution.Distinct ().ToArray ();

		resolutionSlider.maxValue = res.Length - 1;
		resolutionSlider.wholeNumbers = true;

		qualitySlider.maxValue = 5;
		qualitySlider.wholeNumbers = true;

		musicSlider.maxValue = 100;
		musicSlider.wholeNumbers = true;

		soundSlider.maxValue = 100;
		soundSlider.wholeNumbers = true;

		if (!PlayerPrefs.HasKey ("Resolution")) {
			resolutionSlider.value = res.Length - 1;
		}
		else {
			resolutionSlider.value = PlayerPrefs.GetInt ("Resolution");
		}


		qualitySlider.value = QualitySettings.GetQualityLevel ();

		musicSlider.value = PlayerPrefs.GetInt("MusicVolume");
		soundSlider.value = PlayerPrefs.GetInt("SoundVolume");

		fullscreen.isOn = Screen.fullScreen;
	}

	bool IntToBool(int a){
		if (a == 1)
			return true;
		return false;
	}

	int BoolToInt(bool b){
		if (b)
			return 1;
		return 0;
	}

	public void AceptGraphics(){
		QualitySettings.SetQualityLevel ((int)qualitySlider.value);
		Screen.SetResolution (res [(int)resolutionSlider.value].width, res [(int)resolutionSlider.value].height,fullscreen.isOn);
		PlayerPrefs.SetInt ("Quality", (int)qualitySlider.value);
		PlayerPrefs.SetInt ("Resolution", (int)resolutionSlider.value);
		PlayerPrefs.SetInt ("Fullscreen", BoolToInt (fullscreen.isOn));
	}

	public void ResoltionSliderChange() {
		resolutionSlider.transform.Find ("Value").GetComponent<Text> ().text = 
			res [(int)resolutionSlider.value].width + "x" + res [(int)resolutionSlider.value].height;
	}

	public void QualitySliderChange() {
		qualitySlider.transform.Find ("Value").GetComponent<Text> ().text = qualityLevels[(int)qualitySlider.value];
	}

	public void MusicSliderChange() {
		musicSlider.transform.Find ("Value").GetComponent<Text> ().text = ((int)musicSlider.value).ToString();
		FindObjectOfType<MusicPlayer> ().audio.volume = ((int)musicSlider.value)/100f;
		PlayerPrefs.SetInt ("MusicVolume", (int)musicSlider.value);
	}

	public void SoundSliderChange() {
		soundSlider.transform.Find ("Value").GetComponent<Text> ().text = ((int)soundSlider.value).ToString();
		PlayerPrefs.SetInt ("SoundVolume", (int)soundSlider.value);
	}
}
