﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DilogueTrigger : MonoBehaviour {
	public Dilogue dilogue;

	public void TriggerDilogue () {
		FindObjectOfType<DilogManager> ().StartDilogue (dilogue);
	}
}
