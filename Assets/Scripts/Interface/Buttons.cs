﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour {

	public void ChangeScene(string sceneName) {
		Application.LoadLevel (sceneName);
	}

	public void ChangeScene(int sceneNumber) {
		Application.LoadLevel (sceneNumber);
	}

	public void Exit () {
		Application.Quit();
	}
}
