﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesBar : MonoBehaviour {
	private Transform[] hearts;

	[SerializeField]
	public int hp = 5;

	private void Awake() {
		hearts = new Transform[5];

		for (int i = 0; i < hearts.Length; i++) {
			hearts [i] = transform.GetChild (i);
		}
	}

	public void Refresh(int newHp) {
		hp = newHp;
		for (int i = 0; i < hearts.Length; i++) {
			if (i < hp)
				hearts [i].gameObject.SetActive (true);
			else
				hearts [i].gameObject.SetActive (false);
		}
	}
}
