﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DilogManager : MonoBehaviour {
	private Queue<string> sentences = new Queue<string> ();

	public Text name;
	public Text dilogueText;

	public bool isStarted;

	public Animator animator;

	public void Update() {
		if (isStarted && Input.GetButtonDown("Space"))
			DisplayNextSentence ();
	}

	public void StartDilogue(Dilogue dilogue) {
		sentences.Clear ();
		isStarted = true;

		name.text = dilogue.name;

		foreach (string sentence in dilogue.sentences) {
			sentences.Enqueue (sentence);
		}

		animator.SetBool ("IsStarted", true);
		DisplayNextSentence ();
	}

	public void DisplayNextSentence() {
		if (Time.timeScale > 0) {
			if (sentences.Count == 0)
				EndDilogue ();
			else {
				string sentence = sentences.Dequeue ();
				dilogueText.text = sentence;
			}
		}
	}

	public void EndDilogue() {
		isStarted = false;
		animator.SetBool ("IsStarted", false);
	}
}
