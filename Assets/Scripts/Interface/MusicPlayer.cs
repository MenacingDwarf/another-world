﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {
	private AudioClip audioClip;

	public AudioSource audio;

	void Awake(){
		audio = GetComponent<AudioSource> ();
		audio.volume = PlayerPrefs.GetInt ("MusicVolume")/100f;
		DontDestroyOnLoad (this);
	}
}
