﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour {
	
	public Text message;

	void Awake() {

		StartCoroutine (ShowMessage (Application.loadedLevelName));
		PlayerPrefs.SetInt ("LastLevel", Application.loadedLevel);
	}

	public IEnumerator ShowMessage(string text) {
		message.text = text;
		message.GetComponent<Animator> ().SetBool ("Visible", true);

		yield return new WaitForSeconds(1.5f);

		message.GetComponent<Animator> ().SetBool ("Visible", false);
	}
}
