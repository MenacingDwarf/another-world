﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : Buttons {

	[SerializeField]
	private Button resumeButton;

	void Start() {
		if (!PlayerPrefs.HasKey ("MusicVolume"))
			PlayerPrefs.SetInt ("MusicVolume", 100);
		if (!PlayerPrefs.HasKey ("SoundVolume"))
			PlayerPrefs.SetInt ("SoundVolume", 100);
		
		if (PlayerPrefs.GetInt ("LastLevel") > 1)
			resumeButton.interactable = true;
		else 
			resumeButton.interactable = false;
	}

	public void NewGame() {
		for (int i = 0; i < Application.levelCount; i++) {
			PlayerPrefs.DeleteKey ("Level" + i.ToString ());
		}
		PlayerPrefs.DeleteKey ("LastLevel");
		PlayerPrefs.DeleteKey ("KeysNumber");
		ChangeScene ("First Appearance");

	}

	public void Resume() {
		ChangeScene (PlayerPrefs.GetInt ("LastLevel"));
	}
}
