﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

	public GameObject pauseMenu;

	public bool isPaused;

	void Awake () {
		isPaused = false;
	}

	void Update () {
		if (Input.GetButtonDown("Esc") && !isPaused) {
			pauseMenu.SetActive (true);
			isPaused = true;
			Time.timeScale = 0;
		} else if (Input.GetButtonDown("Esc") && pauseMenu.active) {
			Resume ();
		}
	}

	public void Resume() {
		pauseMenu.SetActive (false);
		isPaused = false;
		Time.timeScale = 1;
	}

	public void Restart() {
		Time.timeScale = 1;
		Application.LoadLevel (Application.loadedLevel);
	}

	public void Menu() {
		Time.timeScale = 1;
		MusicPlayer music = FindObjectOfType<MusicPlayer> ();
		if (music)
			Destroy (music.gameObject);
		Application.LoadLevel ("Menu");
	}

	public void Quit() {
		Application.Quit ();
	}
}
