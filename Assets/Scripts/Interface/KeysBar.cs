﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeysBar : MonoBehaviour {

	private void Awake() {
		int KeysNum = PlayerPrefs.GetInt ("KeysNumber");
		for (int i = 0; i < KeysNum; i++) {
			transform.GetChild (i).gameObject.SetActive (true);
		}
	}
}
