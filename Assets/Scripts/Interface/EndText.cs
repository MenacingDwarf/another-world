﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndText : MonoBehaviour {
	private MusicPlayer music;
	public AudioClip endMusic;

	private Animator anim;

	void Start () {
		music = FindObjectOfType<MusicPlayer> ();
		if (music) {
			music.audio.Stop ();
			music.audio.clip = endMusic;
			music.audio.Play ();
		}

		anim = GetComponent<Animator> ();
	}

	public void ChangeAnimation() {
		anim.SetBool ("IsEnd", true);
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			EndGame ();
		}
	}

	void EndGame() {
		if (music)
			Destroy (music.gameObject);
		PlayerPrefs.SetInt ("LastLevel", 0);
		Application.LoadLevel ("Menu");
	}
}
