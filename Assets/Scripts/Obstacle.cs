﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D other) {
		Character unit = other.GetComponent<Character> ();

		if (unit && unit.tag == "Player") {
			unit.ReceiveDamage ();
		}
	}
}
