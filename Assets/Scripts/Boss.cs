﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Character {

	private bool isStarted = false;
	private bool enableJump = true;
	private bool enableTurn = true;
	private bool enableSpam = true;
	private bool canStart = false;
	private bool canShowText = false;

	private Monster spawningMonster;

	private Queue<GameObject> monsters = new Queue<GameObject>();

	[SerializeField]
	private Character player;

	[SerializeField]
	private float talkingRadius = 7f;

	private Vector3 direction;

	protected override void Awake ()
	{
		base.Awake ();

		shootRate = 2f;
		direction = -transform.right;
		spawningMonster = Resources.Load<MovableMonster> ("MovableMonster");
	}

	private void FixedUpdate() {
		if (isStarted)
			checkGround ();
	}

	void Update () {
		if (!isStarted && !dilogueManager.isStarted && canStart) {
			isStarted = true;
			canStart = false;
			livesBar.gameObject.SetActive (true);
		}
		if (!isStarted && animator.GetInteger("State") == 0 && !canStart) {
			if (Mathf.Abs (player.transform.position.x - transform.position.x) < talkingRadius && Mathf.Abs (player.transform.position.y - transform.position.y) < 2f) {
				dilogueManager.StartDilogue (dilogue);
				canStart = true;
			}
		}
		if (isStarted) {
			sprite.flipX = transform.position.x > player.transform.position.x;

			float horizontal = GetComponent<BoxCollider2D> ().size.x / 2 + player.GetComponent<BoxCollider2D> ().size.x;
			float vertical = Mathf.Max (GetComponent<BoxCollider2D> ().size.y, player.GetComponent<BoxCollider2D> ().size.y);

			if (Mathf.Abs (player.transform.position.x - transform.position.x) < horizontal && player.transform.position.y - transform.position.y < vertical)
				player.ReceiveDamage ();

			if (enableShoot)
				StartCoroutine (Shoot (Color.yellow));

			if (enableJump && isGrounded)
				StartCoroutine (Jump (jumpForse));

			if (enableSpam)
				StartCoroutine (Spam ());

			Run ();
		}
		if (canShowText&& !dilogueManager.isStarted) {
			ShowMessage ("--> Go ahead! -->");
			canShowText = false;
		}
	}

	public IEnumerator Jump(float force) {
		float jumpRate = 2f + 2f * Random.value;
		enableJump = false;
		audio.clip = jumpSound;
		audio.Play ();

		rb2D.velocity = Vector3.zero;
		rb2D.AddForce (transform.up * force, ForceMode2D.Impulse);

		yield return new WaitForSeconds (jumpRate);
		enableJump = true;
	}

	private IEnumerator Spam() {
		Vector3 direction = transform.right * ((transform.position.x > player.transform.position.x) ? -1f : 1f);
		Vector3 position = transform.position + direction * 10f + transform.up * 5f;

		MovableMonster monster = Instantiate (spawningMonster, position, spawningMonster.transform.rotation) as MovableMonster;
		monster.Direction = transform.right * ((monster.transform.position.x > player.transform.position.x) ? -1f : 1f);
		monsters.Enqueue (monster.gameObject);

		enableSpam = false;
		yield return new WaitForSeconds (4f);

		enableSpam = true;
	}

	protected override void Run ()
	{
		if (isGrounded) animator.SetInteger ("State", 1);

		if (enableTurn)
			StartCoroutine (Turn ());
		
		transform.position = Vector3.MoveTowards (transform.position, transform.position + direction, speed * Time.deltaTime);
	}

	private IEnumerator Turn(){
		float changeRate = 1f + 2f * Random.value;
		enableTurn = false;
		direction *= -1f;

		yield return new WaitForSeconds (changeRate);
		enableTurn = true;
	}

	public override void ReceiveDamage ()
	{
		if (isStarted)
			base.ReceiveDamage ();
	}

	protected override void Die ()
	{
		if (isStarted) {
			livesBar.gameObject.SetActive (false);
			gameObject.tag = "Player";
			animator.SetInteger ("State", 3);
			isStarted = false;
		}
	}

	private void BossDeath () {
		ShowMessage (dilogue.name + " is dead!");
		animator.SetInteger ("State", 4);
		rb2D.velocity = Vector3.zero;

		gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
		gameObject.GetComponent<Rigidbody2D> ().bodyType = RigidbodyType2D.Kinematic;

		foreach (GameObject monster in monsters)
			Destroy (monster);

		dilogue.sentences = new string[3];
		dilogue.sentences [0] = "You win this battle, but you can't escape!";
		dilogue.sentences [1] = "It is impossible to leave this world!";
		dilogue.sentences [2] = "Argh...";
		dilogueManager.StartDilogue (dilogue);
		canShowText = true;
		player.hasKey = true;
	}
}